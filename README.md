<div align="center">
<img src="https://codeberg.org/vidzy/vidzy/raw/branch/main/static/logo.png">
<h1>Vidzy Desktop App</h1>
The Desktop App for Vidzy

<a href="https://vidzy.codeberg.page/">Website</a>
&nbsp;•&nbsp;
<a href="https://matrix.to/#/#vidzysocial:fedora.im">Matrix</a>
</div>

## License

Vidzy is open-source software licensed under the GNU General Public License v3.0