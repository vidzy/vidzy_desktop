import sys, os
import PyQt5
import PyQt5.QtGui
from PyQt5.Qt import *
from PyQt5.QtWebEngineWidgets import *
from PyQt5.QtWidgets import QApplication
from tkinter import simpledialog
import tkinter as tk
import requests
import json
import tkinter.messagebox

basedir = os.path.dirname(__file__)

if not os.path.exists(os.path.expanduser('~/.vidzy')):
    os.mkdir(os.path.expanduser('~/.vidzy'))

if not os.path.isfile(os.path.expanduser('~/.vidzy/INSTANCE')):
    tk_root = tk.Tk()
    tk_root.withdraw()

    vidzy_instance_to_write = simpledialog.askstring(title="Vidzy", prompt="Enter Instance URL: ")

    try:
        instance_info = json.loads(requests.get(vidzy_instance_to_write + "/api/v1/instance").text)
    except json.decoder.JSONDecodeError:
        print("[Error] URL is not a Vidzy instance")
        tkinter.messagebox.showerror("Error", "URL is not a Vidzy instance")
        exit()

    with open(os.path.expanduser('~/.vidzy/INSTANCE'), "w") as f:
        f.write(vidzy_instance_to_write)

with open(os.path.expanduser('~/.vidzy/INSTANCE'), "r") as f:
    vidzy_instance = f.read()

app = QApplication(sys.argv)
app.setApplicationName("Vidzy")
app.setWindowIcon(QIcon(os.path.join(basedir, 'square_logo.png')))

w=QWidget()
w.showMaximized()

layout = QVBoxLayout(w)

web = QWebEngineView(w)
web.load(QUrl(vidzy_instance))
web.show()

layout.addWidget(web)

w.setLayout(layout)

sys.exit(app.exec_())